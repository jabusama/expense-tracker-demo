import React from 'react';
import globalHook from 'use-global-hook'; 
import initialState from './initialState';
import actions from './actions';

const useGlobal = globalHook(React, initialState, actions);

export default useGlobal;