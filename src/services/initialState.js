
const initialStates = {
  category: [
    {
      id: 'category 1',
      name: "Category 1"
    },
    {
      id: 'category 2',
      name: "Category 2"
    },
    {
      id: 'category 3',
      name: "Category 3"
    },
    {
      id: 'category 4',
      name: "Category 4"
    }
  ],
  expenses: [
    {
      title: 'May 27th 2020 - Category 1',
      amount: '150',
      category: 'category 1',
      date: '2020-05-27T22:11:37+08:00',
      notes: 'notes notes'
    },
    {
      title: 'May 29th 2020 - Category 4',
      amount: '100',
      category: 'category 4',
      date: '2020-05-27T22:11:37+08:00',
      notes: 'notes notes'
    },
    {
      title: 'May 28th 2020 - Category 2',
      amount: '50',
      category: 'category 2',
      date: '2020-05-28T22:11:37+08:00',
      notes: 'notes notes'
    },
    {
      title: 'April 30th 2020 - Category 3',
      amount: '60',
      category: 'category 3',
      date: '2020-04-30T22:11:37+08:00',
      notes: 'notes notes'
    },
    {
      title: 'April 29th 2020 - Category 1',
      amount: '300',
      category: 'category 3',
      date: '2020-04-29T22:11:37+08:00',
      notes: 'notes notes'
    }
  ],

  notificationMessage:		{ error: '', warning: '', success: '' }
}

export default initialStates;