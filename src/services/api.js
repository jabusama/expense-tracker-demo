import axios from 'axios'

export const apiUrl = 'http://localhost:9000/api/'

export const apiRequest = async(method = 'GET', url = '', data = {}, header = {}) => {
	try {
		return axios({
			method,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				...header
			},
			url: apiUrl + url,
			data
		}).then(res => {
			return res
		}).catch(err => {
			return err.response
		})
	} catch (e) {
		console.log('API request Failed')
		return false
	}
}
