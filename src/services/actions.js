// import { apiRequest } from './api';

const actions = {

  addExpenses: async (store, data) => {
    store.state.expenses.unshift(data);
    console.log(store.state.expenses);
    //moct request only
    //because there is no api url
    // const res = await apiRequest('POST', 'add-epenses', data) //uncommment if there is an api
    store.setState({
      expenses: store.state.expenses,
      notificationMessage: {
        success: 'Successfully added',
        error: '',
        warning: ''
      }
    })
  }
}

export default actions;