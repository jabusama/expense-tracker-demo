import React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default function Form({ values, handler, dateHandler, valid, category}) {

  return (
    <div>
      <form>

        <div className="form-row">
          <label htmlFor="" className="col-4 col-form-label d-flex align-items-center">
            Amount
          </label>
          <div className="col-8 amount">
            <input 
              type="text"
              name="amount"
              placeholder="-$0" 
              className={`form-control text-danger text-right border-0 shadow-none ${valid ? '' : 'is-invalid'}`} 
              value={ values.amount }
              onChange={ handler }
            />
            <div className="invalid-feedback text-right">
              This field is required!
            </div>
          </div>
        </div>

        <div className="text-muted">
          <h6 className="h6">Settings</h6>
        </div>
        <hr className="mt-0 mb-2"/>

        <div className="form-row">
          <label htmlFor="category" className="col-4 col-form-label">
            Category
          </label>
          <div className="col-8">
            <select 
              className="form-control border-0 shadow-none btn-link text-right custom-color" 
              name="category"
              value={ values.category }
              onChange={ handler }
            >
              {
                category.map(cat => (
                  <option key={cat.id} value={cat.id}>{ cat.name }</option>
                ))
              }
            </select>
          </div>
        </div>

        <hr className="mt-0 mb-2"/>

        <div className="form-row">
          <label htmlFor="category" className="col-4 col-form-label">Date</label>
          <div className="col-8 text-right">
            <DatePicker 
              className="form-control border-0 shadow-none btn-link text-right custom-color"
              name="category"
              selected={values.date} 
              dateFormat="MMMM d, yyyy"
              onChange={ dateHandler } 
            />
          </div>
        </div>

        <hr className="mt-0"/>

        <div className="form-group">
          <label htmlFor="notes">Notes</label>
          <textarea 
            className="form-control" 
            name="notes" 
            rows="3"
            value={ values.notes }
            onChange={ handler }
          >
          </textarea>
        </div>

      </form>
    </div>
  )
}
