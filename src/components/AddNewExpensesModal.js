import React, { useState } from 'react';
import Form from './Form';
import Notification from '../components/Notification';
import { useForm } from '../custom-hooks/useForm';
import useGLobal from '../services/useGlobal';
import moment from 'moment';

export default function AddForm() {


  const [gStates, gActions] = useGLobal();

  const initValues = {
    title: '',
    amount: '',
    category: 'category 1',
    date: new Date(),
    notes: '',
  }

  const [values, handler, date, reset] = useForm(initValues)
  const [valid, setValid] = useState(true);
  const [notf, setNotf] = useState(false);

  const onSubmitHandler = (e) => {
    if(checkValidity()) {
      const data = values;
      data.date = moment(values.date).format(); //just to convert date to string since where not saving to database
      data.title = `${moment(data.date).format('MMMM Do YYYY')} - ${data.category.charAt(0).toUpperCase() + data.category.slice(1)}` //add the title
      gActions.addExpenses(data);
      setNotf(true);
      reset();
    }
  }

  const checkValidity = () => {
    if(values.amount !== '') {
      setValid(true);
      return true
    }
    else {
      setValid(false)
      return false;
    }
  }

  const notfStatus = () => {
		setNotf(false);
	}

  return (
    <div 
      className="modal fade" 
      id="exampleModal" 
      tabIndex="-1" 
      role="dialog" 
      aria-labelledby="exampleModalLabel" 
      aria-hidden="true"
    >
      {notf && <Notification state={gStates.notificationMessage} isDone={notfStatus} />}
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <button 
              className="btn btn-link custom-color"
              data-dismiss="modal" 
              aria-label="Close"
            >
              Cancel
            </button>
            <h5 
              className="modal-title"
              id="exampleModalLabel"
            >
              Expense
            </h5>
            <button 
              type="submit"
              onClick={ onSubmitHandler }
              className="btn btn-link custom-color" 
            >
              Done
            </button>
          </div>
          <div className="modal-body">
            <Form 
              values= { values } 
              handler={ handler } 
              dateHandler={ date } 
              valid={valid} 
              category={gStates.category}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
