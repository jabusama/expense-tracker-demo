import React, { useState, useEffect } from 'react';

export default function ListExpensesHeader({searchData}) {

  const initState = {
    search: ''
  }

  const [state, setState ] = useState(initState);

  useEffect(() => {
    searchData(state.search);
  },[state])

  const searchHandler = (e) => {
    let search = e.target.value;
    setState(() => ({
      ...state,
      search: search
    }));
  }

  return (
    <div className="mb-2">
      <form >
        <div className="form-group white-placeholder search fontuser shadow-sm">
          <input 
            type="text" 
            placeholder="Title"
            value={state.search} 
            aria-describedby="button-search" 
            className="form-control border-0 text-white shadow-none text-center bg-transparent"
            onChange= {searchHandler}
          />
          <i className="fa fa-search"></i>
        </div>

      </form>

      <div className="text-center">
        <button 
          className="btn btn-primary shadow-sm"
          data-toggle="modal" 
          data-target="#exampleModal" 
        >Add New</button>
      </div>
    </div>
  )
}
