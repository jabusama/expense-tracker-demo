import React from 'react'

export default function ExpensesList({ data }) {

  return (
    <div className="bg-light table-wrapper-scroll-y my-custom-scrollbar">
      <table className="table table-hover">
        <tbody>
          {
            data.map((expense, i) => (
              <tr key={i}>
                <th className='col'>
                  <i className="fas fa-donate mr-1 custom-color"></i>
                  { expense.title }
                </th>
                <td className="text-right text-danger h5">
                  <strong>${expense.amount}</strong>
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  )
}
