import React from 'react';
import { NavLink, Link } from 'react-router-dom';

export default function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-transparent navbar-dark">
      <div className="container">
        <Link className="navbar-brand" to="/">Expense Tracker</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">

            <li className="nav-item active">
              
              {
                window.location.pathname === '/' ? 
                  <NavLink className="nav-link" to="/expenses-list">
                    <i className="fas fa-clipboard-list"></i> Show Expenses <span className="sr-only"></span>
                  </NavLink>
                :
                <p className="nav-link">
                  <i className="fas fa-book-open"></i> List of Expenses <span className="sr-only"></span>
                </p>
              }
              
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
