import Home from './views/home';
import ExpensesList from './views/expenses-list'

const routeLinks = [
  {
    key: 'home',
    path: '/',
    label: 'Home',
    exact: true,
    component: Home
  },
  {
    key: 'expenses-list',
    path: '/expenses-list',
    label: 'Home',
    exact: true,
    component: ExpensesList
  },
]

export default routeLinks;