import React, { useState } from 'react'
import Layout from '../../layouts/Layout';
import useGlobal from '../../services/useGlobal';
import ListExpensesHeader from '../../components/ListExpensesHeader'
import ExpensesList from '../../components/ExpensesList';
import AddNewExpenseModal from '../../components/AddNewExpensesModal'


export default function Expenses() {

  const [gStates] = useGlobal();
  
  const [state, setState] = useState([]);

  const searchData = (data = '') => {
    if (data.length > 2) {
      setState(gStates.expenses.filter(expense => JSON.stringify(expense).search(new RegExp(data, 'ig')) > -1));
    } else {
      setState(gStates.expenses);
    }
  }

  return (
    <Layout>
      <div className="container">
        <ListExpensesHeader searchData={searchData}/>
        <ExpensesList data={state}/>
        <AddNewExpenseModal />
      </div>
    </Layout>
  )
}
