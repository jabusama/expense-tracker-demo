import React from 'react';
import Layout from '../../layouts/Layout';

export default function index() {
  return (
    <Layout>
      <div className="container display-center d-flex justify-content-center align-items-center">
        <h1 className='text-white display-4 text-center'>
          Expense Manager
        </h1>
      </div>
    </Layout>
  )
}
